Once upon a time, snesmusic.org's forum was alive and had constants updates. The forum was killed off and the main approver, YK, stopped approving submissions. There's still some updates to snesmusic.org at the time of this writing (2020), however, they're significantly less frequent...

In any case, on Discord I heard from KungFuFurby he had *a lot* SPC rips that are not on snesmusic.org. I requested he post them on Discord. HCS64's Discord was subsequently flooded with over a hundred Discord messages. The flooded subsided and moved to PM once bxaimc stepped in and asked "What in the deuce happened here?".

This repo contains all the files KungFuFurby shared with me as well as his notes on them. Maybe I will get around to organizing these into sets. I also included LuigiBlood's SPC rips from the Nintendo leaks for good measure.
